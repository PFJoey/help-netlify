import * as messaging from 'firebase/messaging'
import { SnackbarMessage, OptionsObject, SnackbarKey } from 'notistack';
import React from 'react';
import NotificationSnackbar, { INotificationSnackbar } from '../components/NotificationSnackbar';
import { initializeApp } from "firebase/app";

export const uploadFirebaseToken = (token: string) => {
  console.log(token);
}

export const displayDesktopNotification = (message: messaging.MessagePayload) => {
  navigator.serviceWorker.getRegistration('/firebase-cloud-messaging-push-scope').then(req => {
    const notificationTitle = message.notification?.title;
    const notificationOptions = {
      body: message.notification?.body,
      icon: message.notification?.image
    };
    req?.showNotification(notificationTitle!, notificationOptions)
  })
}

export const displaySnackbarNotification = (message: messaging.MessagePayload, enqueueSnackbar: (message: SnackbarMessage, options?: OptionsObject | undefined) => SnackbarKey) => {
  enqueueSnackbar(message?.notification, {
    variant: 'success',
    anchorOrigin: {
      vertical: 'bottom',
      horizontal: 'right',
    },
    content: (key, message: INotificationSnackbar) => <NotificationSnackbar id={key} message={message} />
  })
}

const firebaseConfig = { //from firebase project setting
  apiKey: "X",
  authDomain: "X",
  projectId: "X",
  storageBucket: "X",
  messagingSenderId: "X",
  appId: "X",
  measurementId: "X"
};
export const vapidKey = 'X'

export const app = initializeApp(firebaseConfig);
export const instance = messaging.getMessaging(app);