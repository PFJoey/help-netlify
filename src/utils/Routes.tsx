import React, { lazy, useContext, useEffect, useState } from "react";
import { Navigate, useLocation, useNavigate, useRoutes } from "react-router-dom";
const Dashboard = lazy(() => import("../pages/Dashboard"));
const Login = lazy(() => import("../pages/Login"));
const Register = lazy(() => import("../pages/Register"));
const Users = lazy(() => import("../pages/Users"));
const ForgetPassword = lazy(() => import("../pages/ForgetPassword"));
const PasswordReset = lazy(() => import("../pages/PasswordReset"));
const Profile = lazy(() => import("../pages/Profile"));

import * as messaging from "firebase/messaging";
import {
  displaySnackbarNotification,
} from "../utils/Firebase";
import { instance } from "../utils/Firebase";
import { useSnackbar } from "notistack";
import Layout from "../components/Layout";
import jwt_decode from "jwt-decode";
import { AuthContext, IToken, IUser } from "../contexts/AuthProvider";

const Routes = () => {
  const { enqueueSnackbar } = useSnackbar();
  let navigate = useNavigate();
  let location = useLocation();
  const { user, setUser } = useContext(AuthContext)


  useEffect(() => {
    messaging.onMessage(instance, (message) => {
      displaySnackbarNotification(message, enqueueSnackbar);
      // displayDesktopNotification(message)
    });
  }, []);

  useEffect(() => {
    function checkToken() {
      const token = localStorage.getItem('token')
      
      if (token) {
        try {
          const decoded = jwt_decode<IToken>(token);
          const user: IUser = {
            first_name: decoded.first_name,
            last_name: decoded.last_name,
            id: decoded.sub,
            email: decoded.email,
            role: decoded["https://hasura.io/jwt/claims"]['x-hasura-default-role']
          }
          setUser(user);
        } catch (e) {
          navigate('/login')
          setUser(null);
        }
      } else {
        navigate('/login')
        setUser(null);
      }
    }
    checkToken()
    window.addEventListener('storage', checkToken)

    return () => {
      window.removeEventListener('storage', checkToken)
    }
  }, [location.pathname])


  let routes = useRoutes([
    {
      path: "/",
      element: <Layout />,
      children: [
        {
          path: "/",
          element: (
            <Dashboard />
          ),
        },
        {
          path: "/users",
          element: (
            <Users />
          ),
        },
        {
          path: "/profile",
          element: (
            <Profile />
          ),
        },
      ]
    },
    {
      path: "/login",
      element: <Login />
    },
    {
      path: "/register",
      element: <Register />
    },
    {
      path: "/forget-password",
      element: <ForgetPassword />
    },
    {
      path: "/password-reset/:user_id/:token",
      element: <PasswordReset />
    },
    {
      path: "*",
      element: <Navigate to={'/'} />
    }
  ]);
  return routes;
};

export default Routes;
