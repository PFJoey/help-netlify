interface ImportMetaEnv {
  VITE_GRAPHQL: string;
  VITE_FIREBASE_API_KEY: string;
  VITE_FIREBASE_PROJECT_ID: string;
  VITE_FIREBASE_SENDER_ID: string;
  VITE_FIREBASE_APP_ID: string;
  VITE_PUBLIC_KEY: string;
}
