import React from "react";
import MUIDataTable, { MUIDataTableOptions } from "mui-datatables";
import { useQuery } from "@apollo/client";
import { GET_USERS } from "../graphql/Users.graphql";
import Loading from "./Loading";
import {  Paper } from "@mui/material";

const columns = [
  {
    name: "id",
    label: "ID",
    options: {
      filter: true,
      sort: true,
    },
  },
  {
    name: "email",
    label: "Email",
    options: {
      filter: true,
      sort: true,
    },
  },
  {
    name: "first_name",
    label: "First name",
    options: {
      filter: true,
      sort: true,
    },
  },
  {
    name: "last_name",
    label: "Last name",
    options: {
      filter: true,
      sort: true,
    },
  },
  {
    name: "role",
    label: "Role",
    options: {
      filter: true,
      sort: true,
    },
  },
];

const UserTable = ({ className }: { className?: any }) => {
  const {data, loading } = useQuery(GET_USERS);
  const options: MUIDataTableOptions = {
    filterType: "checkbox",
    elevation: 0,
  };

  if (loading) {
    return <Loading />
  }

  return (
    <Paper className={className}>
      <MUIDataTable
        title={"User list"}
        data={data.auth_user}
        columns={columns}
        options={options}
      />
    </Paper>
  );
};

export default UserTable;
