import { Avatar, IconButton, Menu, MenuItem } from '@mui/material';
import React from 'react'
import { useNavigate } from 'react-router';

const ProfileButton = () => {

    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const open = Boolean(anchorEl);
    const navigate = useNavigate()
    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleAccount = () => {
        setAnchorEl(null);

        navigate("/profile");
    }

    const handleLogout = () => {
        handleClose()
        localStorage.removeItem('token')
        window.dispatchEvent(new Event('storage'))
    }

    return (
        <div>
            <IconButton
                color="inherit"
                aria-expanded={open ? 'true' : undefined}
                onClick={handleClick}>
                <Avatar
                    alt="Remy Sharp"
                    style={{ width: 24, height: 24 }}
                // sx={{ width: 24, height: 24 }}
                />
            </IconButton>

            <Menu
                id="basic-menu"
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                MenuListProps={{
                    'aria-labelledby': 'basic-button',
                }}
            >
                <MenuItem onClick={handleAccount}>My account</MenuItem>
                <MenuItem onClick={handleLogout}>Logout</MenuItem>
            </Menu>
        </div>
    );
}

export default ProfileButton