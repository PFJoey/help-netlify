import React, { useState, forwardRef, useCallback } from 'react';
import classnames from 'classnames';
import { useSnackbar, SnackbarContent } from 'notistack';
import { Card, CardActions, Collapse, IconButton, Paper, Theme, Typography } from '@mui/material';
import {makeStyles} from '@mui/styles'
import { Close, ExpandMore } from '@mui/icons-material';


const useStyles: any = makeStyles((theme: any) => ({
    root: {
        [theme.breakpoints.up('sm')]: {
            minWidth: '344px !important',
        },
    },
    card: {
        backgroundColor: '#fddc6c',
        width: '100%',
    },
    typography: {
        fontWeight: 'bold',
    },
    actionRoot: {
        padding: '8px 8px 8px 16px',
        justifyContent: 'space-between',
    },
    icons: {
        marginLeft: 'auto',
    },
    expand: {
        padding: '8px 8px',
        transform: 'rotate(0deg)',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    collapse: {
        padding: 16,
    },
    checkIcon: {
        fontSize: 20,
        color: '#b3b3b3',
        paddingRight: 4,
    },
    button: {
        padding: 0,
        textTransform: 'none',
    },
}));

export interface INotificationSnackbar {
    title: string,
    body: string
}

const NotificationSnackbar = forwardRef<HTMLDivElement, { id: string | number, message: INotificationSnackbar }>((props, ref) => {
    const classes = useStyles();
    const { closeSnackbar } = useSnackbar();
    const [expanded, setExpanded] = useState(false);

    const handleExpandClick = useCallback(() => {
        setExpanded((oldExpanded) => !oldExpanded);
    }, []);

    const handleDismiss = useCallback(() => {
        closeSnackbar(props.id);
    }, [props.id, closeSnackbar]);

    return (
        <SnackbarContent ref={ref} className={classes.root}>
            <Card className={classes.card}>
                <CardActions classes={{ root: classes.actionRoot }}>
                    <Typography variant="subtitle2" className={classes.typography}>{props.message.title}</Typography>
                    <div className={classes.icons}>
                        <IconButton
                            aria-label="Show more"
                            className={classnames(classes.expand, { [classes.expandOpen]: expanded })}
                            onClick={handleExpandClick}
                        >
                            <ExpandMore />
                        </IconButton>
                        <IconButton className={classes.expand} onClick={handleDismiss}>
                            <Close />
                        </IconButton>
                    </div>
                </CardActions>
                <Collapse in={expanded} timeout="auto" unmountOnExit>
                    <Paper className={classes.collapse}>
                        <Typography gutterBottom>{props.message.body}</Typography>
                        
                    </Paper>
                </Collapse>
            </Card>
        </SnackbarContent>
    );
});

export default NotificationSnackbar;