import React from 'react'
import { Link as RouterLink } from 'react-router-dom';
import Link, { LinkProps } from '@material-ui/core/Link';
import { Apps, Business, PeopleAlt, Security, Home, Person } from '@mui/icons-material';
import { Breadcrumbs, Typography } from '@mui/material';

const breadcrumbNameMap: { [key: string]: string } = {
    '/profile': 'Profile',
    '/': 'Dashboard',
    '/users': 'Users',

};

const iconMap: any = {
    'users': <PeopleAlt style={{ marginRight: 5 }} />,
    'profile': <Person style={{ marginRight: 5 }} />,
}


const LinkRouter = (props: LinkRouterProps) => <Link {...props} component={RouterLink as any} />;

interface LinkRouterProps extends LinkProps {
    to: string;
    replace?: boolean;
}

const RouterCrumbs = () => {
    const pathnames = location.pathname.split('/').filter((x) => x);
    return (
        <Breadcrumbs aria-label="breadcrumb" style={{ marginBottom: 20 }}>
            {pathnames.length === 0 && (
                <LinkRouter color="textPrimary" to="/" style={{ display: 'flex' }}>
                    <Home style={{ marginRight: 5 }} />
                    Dashboard
                </LinkRouter>
            )}


            {pathnames.map((value, index) => {
                const last = index === pathnames.length - 1;
                const to = `/${pathnames.slice(0, index + 1).join('/')}`;
                const isParameter = !!!breadcrumbNameMap[to]

                if (isParameter) {
                    return <Typography color="textPrimary" key={to}>
                        {value}
                    </Typography>
                }

                return last ? (
                    <Typography color="textPrimary" key={to} style={{ display: 'flex' }}>

                        {iconMap[value]}

                        {breadcrumbNameMap[to]}
                    </Typography>
                ) : (
                    <LinkRouter color="inherit" to={to} key={to} style={{ display: 'flex' }} >
                        {iconMap[value]}
                        {breadcrumbNameMap[to]}
                    </LinkRouter>
                );
            })}
        </Breadcrumbs>
    )
}

export default RouterCrumbs