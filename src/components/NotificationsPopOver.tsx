import React from 'react'
import Popover from '@mui/material/Popover';
import Typography from '@mui/material/Typography';
import { Badge, IconButton } from '@mui/material';
import { Notifications, NotificationsActive } from '@mui/icons-material';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Divider from '@mui/material/Divider';
import InboxIcon from '@mui/icons-material/Inbox';
import DraftsIcon from '@mui/icons-material/Drafts';

const NotificationsPopOver = () => {
    const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);

    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const open = Boolean(anchorEl);
    const id = open ? 'simple-popover' : undefined;

    return (
        <div>

            <IconButton
                color="inherit"
                aria-label="open settings"
                aria-describedby={id}
                // onClick={handleDrawerOpen}
                onClick={handleClick}
            >
                <Badge badgeContent={2} color="secondary">

                    <NotificationsActive />
                </Badge>

            </IconButton>

            <Popover
                id={id}
                open={open}
                anchorEl={anchorEl}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                }}
            >
                <List>
                    <ListItem disablePadding dense>
                        <ListItemButton>
                            <ListItemText primary="Notificatie #1" secondary="Klik hier om er heen te gaan" />
                        </ListItemButton>
                    </ListItem>
                    <Divider />
                    <ListItem disablePadding dense>
                        <ListItemButton>
                            <ListItemText primary="Notificatie #2" secondary="Klik hier om er heen te gaan" />
                        </ListItemButton>
                    </ListItem>
                    {/* <Divider /> */}
                    {/* <Typography sx={{ p: 2 }}>Je hebt 2 notificaties!</Typography> */}
                </List>
            </Popover>
        </div>
    );
}

export default NotificationsPopOver