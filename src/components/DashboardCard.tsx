import React from "react";
import {
  Card,
  CardActionArea,
  CardMedia,
  CardContent,
  Typography,
} from "@mui/material";
import { useNavigate } from "react-router-dom";

const DashboardCard = ({
  title,
  image,
  routerName,
}: {
  title: string;
  image: string;
  routerName: any;
}) => {
  const navigate = useNavigate();

  return (
    <Card sx={{ maxWidth: 400, minWidth: 200 }}>
      <CardActionArea onClick={() => navigate(routerName)}>
        <CardMedia component="img" height="100" src={image} />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            {title}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

export default DashboardCard;
