import React from 'react'
import { LinearProgress, Theme } from '@material-ui/core';
import { withStyles, createStyles } from '@material-ui/core/styles';

const BorderLinearProgress = withStyles((theme: Theme) =>
    createStyles({
        root: {
            height: 10,
            borderRadius: 5,
        },
        colorPrimary: {
            backgroundColor: theme.palette.grey[theme.palette.type === 'light' ? 200 : 700],
        },
        bar: {
            borderRadius: 5,
            backgroundColor: theme.palette.primary.main,
        },
    }),
)(LinearProgress);

const Loading = () => {
    return (
        <div style={{ display: 'flex', flexDirection: 'column', width: '100%', justifyContent: 'center', alignItems: 'center', gap: 60, height: '100%', paddingTop: "10vw" }}>
            <BorderLinearProgress color="primary" style={{ width: '50%' }} />
        </div>
    )
}

export default Loading
