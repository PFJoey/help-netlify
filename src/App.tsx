import React, { Suspense, useState } from "react";
import "./App.css";
import { BrowserRouter } from "react-router-dom";
import AuthorizedApolloProvider from "./contexts/AuthorizedApolloProvider";
import useDarkMode from "./hooks/useDarkMode";
import { dark, light } from "./utils/MuiTheme";
import Routes from "./utils/Routes";
import { SnackbarProvider } from "notistack";
import { ThemeProvider } from "@mui/material/styles";
import { CssBaseline } from "@mui/material";
import Loading from "./components/Loading";
import { AuthContext, IUser } from './contexts/AuthProvider'
function App() {
  const { isDarkMode } = useDarkMode();
  const [user, setUser] = useState<IUser | null>(null)

  return (
    <BrowserRouter>
      <ThemeProvider theme={isDarkMode ? dark() : light()}>
        <SnackbarProvider maxSnack={3}>
          <CssBaseline />
          <AuthContext.Provider
            key={'authentication_provider'}
            value={{ user, setUser }}
          >
            <AuthorizedApolloProvider>
              <Suspense fallback={<Loading />}>
                <Routes />
              </Suspense>
            </AuthorizedApolloProvider>
          </AuthContext.Provider>
        </SnackbarProvider>
      </ThemeProvider>
    </BrowserRouter>
  );
}

export default App;
