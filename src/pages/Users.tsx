import React from "react";
import { makeStyles } from "@mui/styles";
import UserTable from "../components/UserTable";

const useStyles = makeStyles((theme: any) => ({}));

const Users = () => {
  const classes = useStyles();

  return (
    <UserTable />
  );
};

export default Users;
