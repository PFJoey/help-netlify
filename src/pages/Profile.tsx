import { TextField, Typography, Divider, Button } from '@mui/material';
import { useFormik } from 'formik';
import React, { useContext } from 'react'
import RouterCrumbs from '../components/RouterCrumbs';
import * as Yup from 'yup'
import { useMutation, useQuery } from '@apollo/client';
import { CHANGEPASSWORD, GET_USER, UPDATEUSER } from '../graphql/Users.graphql';
import { useSnackbar } from 'notistack';
import DebouncedButton from '../components/DebounceButton';
import { AuthContext } from '../contexts/AuthProvider';

const detailsSchema = Yup.object({
    first_name: Yup.string().label('First name').required('First name is required').ensure(),
    last_name: Yup.string().label('Last name').required('Last name is required').ensure(),
    email: Yup.string().email().label('Email').required('Email is required').ensure(),
})

const passwordSchema = Yup.object({
    password: Yup.string().label('Password').required('Password is required').ensure(),
    new_password: Yup.string().label('New password').required('New password is required').notOneOf([Yup.ref('password')], 'Password should not be the same as the current password').min(8, 'Password must be at least 8 characters').ensure(),
    confirm_password: Yup.string().label('Confirm new password').notOneOf([Yup.ref('password')], 'Password should not be the same as the current password').oneOf([Yup.ref('new_password')], 'Password does not match').required('Required')
})

const getTouched = (formik: any) => {
    let edited = {} as any
    for (const key of Object.keys(formik.touched)) {
        if (formik.values[key] !== formik.initialValues[key]) {
            edited[key] = formik.values[key]
        }
    }
    return {
        edited,
        changed: Object.keys(edited).length > 0
    }
}

const Profile = () => {
    const { enqueueSnackbar } = useSnackbar();
    const { user, setUser } = useContext(AuthContext)
    const {data: profile, loading} = useQuery(GET_USER, {
        variables: {
            id: user?.id
        }
    })

    const [changePassword, { loading: changeLoading }] = useMutation(CHANGEPASSWORD, {
        onCompleted: (data) => {
            enqueueSnackbar(data.changePassword.message, { variant: 'success' })
            changePasswordForm.resetForm()
        }
    });
    const [updateUser, { loading: updateLoading }] = useMutation(UPDATEUSER, {
        onCompleted: (data) => {
            enqueueSnackbar('Succesfully changed user details', { variant: 'success' })
            let newUser = JSON.parse(JSON.stringify(user))
            newUser.first_name = data.update_auth_user_by_pk.first_name
            newUser.last_name = data.update_auth_user_by_pk.last_name
            newUser.email = data.update_auth_user_by_pk.email
            setUser(newUser)
        }
    });
    

    const details = useFormik({
        validateOnMount: true,
        enableReinitialize: true,
        initialValues: {
            first_name: loading ? '' : profile?.auth_user_by_pk?.first_name,
            last_name: loading ? '' : profile?.auth_user_by_pk?.last_name,
            email: loading ? '' : profile?.auth_user_by_pk?.email,
        },
        validationSchema: detailsSchema,
        onSubmit: (values) => {
            updateUser({
                variables: {
                    id: user?.id,
                    _set: getTouched(details).edited
                }
            })
            
        },
    })

    const changePasswordForm = useFormik({
        validateOnMount: true,
        initialValues: {
            password: '',
            new_password: '',
            confirm_password: '',
        },
        validationSchema: passwordSchema,
        onSubmit: (values) => {
            changePassword({
                variables: {
                    password: values.new_password,
                    current_password: values.password
                }
            })
        },
    })

    if (loading) {
        return <div>loading</div>
    }


    return (
        <div style={{ display: 'flex', flexDirection: 'column', gap: 10 }}>
            <RouterCrumbs />
            <Typography variant="h4">Details</Typography>
            <Divider />

            <TextField
                id="first_name"
                name="first_name"
                variant="outlined"
                label={'First name'}
                onChange={details.handleChange}
                onBlur={details.handleBlur}
                value={details.values.first_name}
                error={details.touched.first_name && Boolean(details.errors.first_name)}
                helperText={details.touched.first_name && details.errors.first_name}
            />
            <TextField
                id="last_name"
                name="last_name"
                variant="outlined"
                label={'Last name'}
                onChange={details.handleChange}
                onBlur={details.handleBlur}
                value={details.values.last_name}
                error={details.touched.last_name && Boolean(details.errors.last_name)}
                helperText={details.touched.last_name && details.errors.last_name}
            />
            <TextField
                id="email"
                name="email"
                variant="outlined"
                label={'Email'}
                onChange={details.handleChange}
                onBlur={details.handleBlur}
                value={details.values.email}
                error={details.touched.email && Boolean(details.errors.email)}
                helperText={details.touched.email && details.errors.email}
            />
            <DebouncedButton
                debounceTime={500}
                variant="contained"
                color="secondary"
                loading={changeLoading}
                onClick={() => details.handleSubmit()}
                disabled={!details.isValid || !getTouched(details).changed || updateLoading}
            >
                Save
            </DebouncedButton>

            <div style={{ margin: 20 }}></div>
            <Typography variant="h4">Authentication</Typography>
            <Divider />
            <TextField
                id="password"
                name="password"
                variant="outlined"
                type="password"
                label={'Current password'}
                onChange={changePasswordForm.handleChange}
                onBlur={changePasswordForm.handleBlur}
                value={changePasswordForm.values.password}
                error={changePasswordForm.touched.password && Boolean(changePasswordForm.errors.password)}
                helperText={changePasswordForm.touched.password && changePasswordForm.errors.password}
            />
            <TextField
                id="new_password"
                name="new_password"
                variant="outlined"
                type="password"
                label={'New password'}
                onChange={changePasswordForm.handleChange}
                onBlur={changePasswordForm.handleBlur}
                value={changePasswordForm.values.new_password}
                error={changePasswordForm.touched.new_password && Boolean(changePasswordForm.errors.new_password)}
                helperText={changePasswordForm.touched.new_password && changePasswordForm.errors.new_password}
            />
            <TextField
                id="confirm_password"
                name="confirm_password"
                variant="outlined"
                type="password"
                label={'Confirm new password'}
                onChange={changePasswordForm.handleChange}
                onBlur={changePasswordForm.handleBlur}
                value={changePasswordForm.values.confirm_password}
                error={changePasswordForm.touched.confirm_password && Boolean(changePasswordForm.errors.confirm_password)}
                helperText={changePasswordForm.touched.confirm_password && changePasswordForm.errors.confirm_password}
            />
            <DebouncedButton
                debounceTime={500}
                variant="contained"
                color="secondary"
                loading={changeLoading}
                onClick={() => changePasswordForm.handleSubmit()}
                disabled={!changePasswordForm.isValid || !getTouched(changePasswordForm).changed || changeLoading}
            >
                Change
            </DebouncedButton>
        </div>
    )
}

export default Profile;