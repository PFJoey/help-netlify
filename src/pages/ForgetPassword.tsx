import React, { useEffect } from "react";
import { CardMedia, Card, TextField, Button, Link } from "@mui/material";
import { useFormik } from "formik";
import * as yup from "yup";
import { useNavigate } from "react-router-dom";
import { makeStyles, useTheme } from "@mui/styles";
import LOGO from "../assets/profitflow-logo.png";
import {
    uploadFirebaseToken,
    displayDesktopNotification,
    displaySnackbarNotification,
} from "../utils/Firebase";
import { vapidKey, instance } from "../utils/Firebase";
import * as messaging from "firebase/messaging";
import { useSnackbar } from "notistack";
import { useMutation } from "@apollo/client";
import { FORGETPASSWORD, LOGIN } from "../graphql/Users.graphql";
import jwt_decode from "jwt-decode";

import Dark from "../assets/logo-dark.png";

import useDarkMode from "../hooks/useDarkMode";
import DebouncedButton from "../components/DebounceButton";
import { Divider } from "@material-ui/core";
const useStyles = makeStyles((theme: any) => ({
    form: {
        width: "100%", // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    media: {
        paddingTop: "30%", // 16:9,
    },
    card: {
        margin: "0 auto",
        padding: 20,
        minWidth: 300,
        width: 350,
    },
    cardItem: {
        margin: "0 auto",
        display: "flex",
        flexDirection: "column",
        gap: 15,
        marginTop: 10,
        width: "100%",
    },
}));

const validationSchema = yup.object({
    email: yup
        .string()
        .email("Enter a valid email")
        .required("Email is required"),
});

const ForgetPassword = () => {
    const classes = useStyles();
    let navigate = useNavigate();
    const { enqueueSnackbar } = useSnackbar();
    const { isDarkMode } = useDarkMode();
    const theme = useTheme<any>()

    useEffect(() => {
        const token = localStorage.getItem('token')
        if (token) {
            try {
                jwt_decode(token);
                navigate('/')
            } catch (e) {
                localStorage.removeItem('token')
            }
        } else {
            localStorage.removeItem('token')
        }
    }, [])

    const [forgetPassword, { data, loading }] = useMutation(FORGETPASSWORD, {
        onCompleted: (data) => {
            enqueueSnackbar("A mail has been sent to your email.", { variant: "success" });
            navigate("/login");
        }
    });

    const formik = useFormik({
        initialValues: {
            email: "",
            password: "",
        },
        validationSchema: validationSchema,
        validateOnMount: true,
        onSubmit: (values) => {
            console.log(values);

            forgetPassword({
                variables: {
                    email: values.email,
                },
            });
        },
    });

    return (
        <div
            style={{
                height: "100%",
                display: "flex",
                alignItems: "center",
                backgroundColor: theme.palette.primary.main
            }}
        >
            <Card className={classes.card}>
                <CardMedia
                    style={{
                        width: "100%",
                        backgroundSize: "contain",
                        backgroundPosition: 'center'
                    }}
                    className={classes.media}
                    image={Dark}
                />

                <form className={classes.cardItem} onSubmit={formik.handleSubmit}>
                    <TextField
                        label="Email"
                        name="email"
                        size="small"
                        variant="outlined"
                        value={formik.values.email}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        error={formik.touched.email && Boolean(formik.errors.email)}
                        helperText={formik.touched.email && formik.errors.email}
                    />
                    <DebouncedButton size="small" debounceTime={500} variant="contained" disabled={!formik.isValid || loading} onClick={() => formik.submitForm()}>
                        Send mail
                    </DebouncedButton>
                    <Divider />
                    <Link href="">Go to login</Link>
                </form>
            </Card>
        </div>
    );
};

export default ForgetPassword;
