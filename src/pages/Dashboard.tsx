import React, { useContext } from "react";
import { makeStyles } from "@mui/styles";
import DashboardCard from "../components/DashboardCard";
import Customers from '../assets/customers.svg'
import { AuthContext } from "../contexts/AuthProvider";
import { Button } from "@mui/material";
const useStyles = makeStyles((theme: any) => ({
  container: {
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap",
    gap: 40,
  },
}));

const Dashboard = () => {
  const classes = useStyles();
  const { user, setUser } = useContext(AuthContext)

  return (
    <>
      <Button onClick={() => console.log(user)}> Test</Button>
      <div className={classes.container}>
        <DashboardCard
          title={"Gebruikers"}
          image={Customers}
          routerName="/users"
        />
      </div>
    </>
  );
};

export default Dashboard;
