import React, { useEffect } from "react";
import { CardMedia, Card, TextField, Button, Link, CircularProgress } from "@mui/material";
import { useFormik } from "formik";
import * as yup from "yup";
import { useNavigate } from "react-router-dom";
import { makeStyles, useTheme } from "@mui/styles";
import LOGO from "../assets/profitflow-logo.png";
import {
    uploadFirebaseToken,
    displayDesktopNotification,
    displaySnackbarNotification,
} from "../utils/Firebase";
import { vapidKey, instance } from "../utils/Firebase";
import * as messaging from "firebase/messaging";
import { useSnackbar } from "notistack";
import { useMutation } from "@apollo/client";
import { LOGIN, REGISTER } from "../graphql/Users.graphql";
import jwt_decode from "jwt-decode";

import Dark from "../assets/logo-dark.png";

import useDarkMode from "../hooks/useDarkMode";
import DebouncedButton from "../components/DebounceButton";
import { Divider } from "@material-ui/core";
const useStyles = makeStyles((theme: any) => ({
    form: {
        width: "100%", // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    media: {
        paddingTop: "30%", // 16:9,
    },
    card: {
        margin: "0 auto",
        padding: 20,
        minWidth: 300,
        width: 350,
    },
    cardItem: {
        margin: "0 auto",
        display: "flex",
        flexDirection: "column",
        gap: 15,
        marginTop: 10,
        width: "100%",
    },
}));

const validationSchema = yup.object({
    email: yup
        .string()
        .email("Enter a valid email")
        .required("Email is required"),
    password: yup
        .string()
        .min(8, 'Password must be atleast 8 characters long')
        .required("Password is required"),
    first_name: yup
        .string()
        .required("First name is required"),
    last_name: yup
        .string()
        .required("First name is required"),
});

const Register = () => {
    const classes = useStyles();
    let navigate = useNavigate();
    const { enqueueSnackbar } = useSnackbar();
    const { isDarkMode } = useDarkMode();
    const theme = useTheme<any>()

    useEffect(() => {
        const token = localStorage.getItem('token')
        if (token) {
            try {
                jwt_decode(token);
                navigate('/')
            } catch (e) {
                localStorage.removeItem('token')
            }
        } else {
            localStorage.removeItem('token')
        }
    }, [])

    const [register, { data, loading }] = useMutation(REGISTER, {
        onCompleted: (data) => {
            localStorage.setItem("token", data.register.token);
            enqueueSnackbar("You have succesfully registered!", { variant: "success" });
            navigate("/");
        }
    });

    const formik = useFormik({
        initialValues: {
            email: "",
            password: "",
            first_name: "",
            last_name: ""
        },
        validationSchema: validationSchema,
        validateOnMount: true,
        onSubmit: (values) => {
            register({
                variables: {
                    email: values.email,
                    password: values.password,
                    first_name: values.first_name,
                    last_name: values.last_name,
                    role: 'admin'
                },
            });
        },
    });

    return (
        <div
            style={{
                height: "100%",
                display: "flex",
                alignItems: "center",
                backgroundColor: theme.palette.primary.main
            }}
        >
            <Card className={classes.card}>
                <CardMedia
                    style={{
                        width: "100%",
                        backgroundSize: "contain",
                        backgroundPosition: 'center'
                    }}
                    className={classes.media}
                    image={Dark}
                />

                <form className={classes.cardItem} onSubmit={formik.handleSubmit}>
                    <TextField
                        label="Email"
                        name="email"
                        size="small"
                        variant="outlined"
                        value={formik.values.email}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        error={formik.touched.email && Boolean(formik.errors.email)}
                        helperText={formik.touched.email && formik.errors.email}
                    />
                    <TextField
                        label="First name"
                        name="first_name"
                        size="small"
                        variant="outlined"
                        value={formik.values.first_name}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        error={formik.touched.first_name && Boolean(formik.errors.first_name)}
                        helperText={formik.touched.first_name && formik.errors.first_name}
                    />
                    <TextField
                        label="Last name"
                        name="last_name"
                        size="small"
                        variant="outlined"
                        value={formik.values.last_name}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        error={formik.touched.last_name && Boolean(formik.errors.last_name)}
                        helperText={formik.touched.last_name && formik.errors.last_name}
                    />
                    <TextField
                        label="Password"
                        name="password"
                        size="small"
                        variant="outlined"
                        type="password"
                        value={formik.values.password}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        error={formik.touched.password && Boolean(formik.errors.password)}
                        helperText={formik.touched.password && formik.errors.password}
                        onKeyPress={(e) => {
                            if (e.key == 'Enter') {
                                formik.submitForm()
                            }
                        }}
                    />
                    <DebouncedButton size="small" debounceTime={500} variant="contained" disabled={!formik.isValid || loading} onClick={() => formik.submitForm()}>
                        {loading ? <CircularProgress size={22} /> : 'Register'}
                    </DebouncedButton>
                    <Divider />
                    <Link href="/login">Already have an account?</Link>
                </form>
            </Card>
        </div>
    );
};

export default Register;
