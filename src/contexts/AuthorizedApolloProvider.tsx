import {
  ApolloClient,
  ApolloProvider,
  createHttpLink,
  from,
  InMemoryCache,
} from "@apollo/client";
import { setContext } from "@apollo/client/link/context";
import { onError } from "@apollo/client/link/error";
import { useSnackbar } from "notistack";
import React from "react";

const AuthorizedApolloProvider = ({ children }: any) => {
  const { enqueueSnackbar } = useSnackbar();
  // const auth = useAuth();
  let uri: string = import.meta.env.VITE_GRAPHQL;

  const httpLink = createHttpLink({
    uri,
  });

  const errorLink = onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors)
      graphQLErrors.forEach(({ message, locations, path }) =>
        enqueueSnackbar(`[API error]: Message: ${message}`, {
          variant: "warning",
        })
      );
    if (networkError)
      enqueueSnackbar(`[Network error]: ${networkError}`, { variant: "error" });
  });

  const authLink = setContext(async () => {
    const token = localStorage.getItem("token");
    
    // check if valid jwt token by decoding using public key ofzo
    // if (token) {
      return {
        headers: {
          Authorization: `Bearer ${token}`,
          "x-hasura-admin-secret": `Welkom01!`,
        },
      };
    // }
  });

  const apolloClient = new ApolloClient({
    link: from([errorLink, authLink.concat(httpLink)]),
    cache: new InMemoryCache(),
    connectToDevTools: true,
  });

  return <ApolloProvider client={apolloClient}>{children}</ApolloProvider>;
};

export default AuthorizedApolloProvider;
