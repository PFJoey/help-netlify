import React from 'react'

export interface IUser {
    id: string;
    first_name: string;
    last_name: string;
    email: string;
    role: string;
}

export interface IToken {
    exp: number,
    iat: number,
    sub: string,
    first_name: string,
    last_name: string,
    email: string,
    "https://hasura.io/jwt/claims": {
        'x-hasura-user-id': string,
        'x-hasura-team-id': string,
        'x-hasura-allowed-roles': string[],
        'x-hasura-default-role': string,
    }
}


export interface IAuthContext {
    user: IUser | null,
    setUser: (user: IUser | null) => void,
}

export const AuthContext = React.createContext<IAuthContext>({
    user: null,
    setUser: (user: IUser | null) => { },
})
