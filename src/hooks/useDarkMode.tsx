import { useEffect } from 'react'

import useLocalStorage from './useLocalStorage'

const COLOR_SCHEME_QUERY = '(prefers-color-scheme: light)'

interface UseDarkModeOutput {
  isDarkMode: boolean
  isSystemPreference: boolean
  toggleDarkMode: () => void
  enableDarkMode: () => void
  disableDarkMode: () => void
  useSystemPreference: () => void
}

function useDarkMode(defaultValue?: boolean): UseDarkModeOutput {
  const getPrefersScheme = (): boolean => {
    // Prevents SSR issues
    if (typeof window !== 'undefined') {
      return window.matchMedia(COLOR_SCHEME_QUERY).matches
    }

    return !!defaultValue
  }

  const [isDarkMode, setDarkMode] = useLocalStorage<boolean>(
    'darkMode',
    getPrefersScheme(),
  )

  // Update darkMode if os prefers changes
  useEffect(() => {
    const handler = () => setDarkMode(getPrefersScheme)
    const matchMedia = window.matchMedia(COLOR_SCHEME_QUERY)

    matchMedia.addEventListener('change', handler)

    return () => {
      matchMedia.removeEventListener('change', handler)
    }
  }, [])

  return {
    isDarkMode,
    isSystemPreference: isDarkMode == getPrefersScheme(),
    toggleDarkMode: () => setDarkMode(prev => !prev),
    enableDarkMode: () => setDarkMode(true),
    disableDarkMode: () => setDarkMode(false),
    useSystemPreference: () => setDarkMode(getPrefersScheme())
  }
}

export default useDarkMode