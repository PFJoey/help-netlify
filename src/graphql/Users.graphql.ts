import gql from "graphql-tag";

export const GET_USERS = gql`
    query GET_USERS {
        auth_user {
            id
            email
            first_name
            last_name
            role
        }
    }
`

export const GET_USER = gql`
    query GET_USER($id: Int!) {
        auth_user_by_pk(id: $id) {
            id
            email
            first_name
            last_name
            role
        }
    }
`


export const LOGIN = gql`
  mutation LOGIN($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      token
    }
  }
`

export const REGISTER = gql`
  mutation REGISTER($email: String!, $password: String!, $last_name: String!, $first_name: String!, $role: String!) {
    register(email: $email, password: $password, last_name: $last_name, first_name: $first_name, role: $role) {
      token
    }
  }
`

export const FORGETPASSWORD = gql`
  mutation FORGETPASSWORD($email: String!) {
    forgetPassword(email: $email) {
      message
    }
  }
`

export const RESETPASSWORD = gql`
  mutation RESETPASSWORD($token: String!, $password: String!, $user_id: Int!) {
    resetPassword(token: $token, password: $password, user_id: $user_id) {
      message
    }
  }
`

export const CHANGEPASSWORD = gql`
  mutation CHANGEPASSWORD($current_password: String!, $password: String!) {
      changePassword(current_password: $current_password, password: $password) {
        message
      }
  }
`

export const UPDATEUSER = gql`
  mutation UPDATEUSER($id: Int!, $_set: auth_user_set_input) {
    update_auth_user_by_pk(pk_columns: {id: $id}, _set: $_set) {
      id
      first_name
      last_name
      email
    }
  }
`

