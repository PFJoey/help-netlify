import passwordComplexity from 'joi-password-complexity';
import Joi from 'joi';
import { Handler } from "@netlify/functions";
import { GraphQLClient } from 'graphql-request';
import { Transporter } from 'nodemailer'
import { parseBody, createResponse } from "./lib/util";
import SMTPTransport from "nodemailer/lib/smtp-transport";
import { changePassword, getToken, initializeHasura, removeResetToken } from "./lib/hasura-util";
import { initializeMail, sendMail } from "./lib/mail-util";
import { parseISO, isBefore, toDate } from 'date-fns'
import { template } from './lib/mail';

let { APP_URL, APP_NAME } = process.env;

let transport: Transporter<SMTPTransport.SentMessageInfo>;

const schema = Joi.object({
    token: Joi.string().required(),
    password: passwordComplexity().required(),
    user_id: Joi.number().required()
})

/**
 * Changes the password through the X valid generated token during forgot password.
 * 
 * Body:
 * @param token: String
 * @param password: String
 * @returns message
 */
const handler: Handler = async (event, context, callback) => {
    const parsedBody = parseBody(event, callback)

    const { error: bodyError, value: validBody } = schema.validate(parsedBody)
    if (bodyError) {
        return createResponse({ statusCode: 400, body: { message: 'Bad request: Missing data.', error: bodyError.details } })
    }

    const { token, password, user_id } = validBody;

    if (!token || !password || !user_id) {
        return createResponse({ statusCode: 400, body: { message: 'Bad request: Missing data.' } })
    }

    let graphql: GraphQLClient = initializeHasura(callback, event, true);

    // Hot-loading implementation (Only initializes while performing cold-starts)
    if (!transport) {
        transport = initializeMail(callback)
    }

    const tokenPayload = await getToken(graphql, { user_id, token })
    if (!tokenPayload) {
        return createResponse({ statusCode: 400, body: { message: 'Bad request: Token does not exist.' } })
    }

    const expiresAt = parseISO(tokenPayload.expires_at);
    const now = toDate(new Date());
    if (isBefore(expiresAt, now)) {
        const removed = await removeResetToken(graphql, { user_id: tokenPayload.user.id });
        if (!removed) {
            // return createResponse({ statusCode: 500, body: { message: 'Internal server error: Something went wrong trying to remove the token' } })
        }
        return createResponse({ statusCode: 400, body: { message: 'Bad request: Token expired.' } })
    }

    const changedPassword = await changePassword(graphql, { user_id: tokenPayload.user.id, password });
    if (!changedPassword) {
        return createResponse({ statusCode: 500, body: { message: 'Internal server error: Something went wrong trying to change the password' } })
    }

    const removed = await removeResetToken(graphql, { user_id: tokenPayload.user.id });
    if (!removed) {
        // return createResponse({ statusCode: 500, body: { message: 'Internal server error: Something went wrong trying to remove the token' } })
    }

    let confirmation = await sendMail(transport, {
        to: tokenPayload.user.email,
        subject: `${APP_NAME} | Your password has been changed`,
        html: template(
            'https://www.myfinance.nl/wp-content/uploads/2018/09/logo-placeholder.png',
            `${tokenPayload.user.first_name}`,
            'Your password has been succesfully changed.',
            ' Please notify an administrator if this was not you.',
            'LOGIN',
            `${APP_URL}/login`,
            'support@profitflow.nl',
            'Maagdenburgstraat 14, Deventer'
        )
    })
    if (!confirmation.messageId) {
        return createResponse({ statusCode: 500, body: { message: 'Internal server error: Mail could not be sent.' } })
    }

    return createResponse({ statusCode: 200, body: { message: 'Password has been succesfully changed!' } })
};

export { handler };