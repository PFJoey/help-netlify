import passwordComplexity from 'joi-password-complexity';
import Joi from 'joi';
import { Handler } from "@netlify/functions";
import { GraphQLClient } from 'graphql-request';
import { Transporter } from 'nodemailer'
import { parseBody, createResponse } from "./lib/util";
import { getUserByEmail, initializeHasura, insertResetToken } from "./lib/hasura-util";
import { initializeMail, sendMail } from "./lib/mail-util";
import SMTPTransport from "nodemailer/lib/smtp-transport";
import { template } from './lib/mail'

let { APP_URL, APP_NAME } = process.env;

let transport: Transporter<SMTPTransport.SentMessageInfo>;


const schema = Joi.object({
    email: Joi.string().email().required(),
})

/**
 * Initiates a password reset by sending an email with a password change link ({APP_URL}/password-reset/{user_id}/{token}) to the user with an X (db depending) expiry date.
 * 
 * Body:
 * @param email: String
 * @returns message
 */
const handler: Handler = async (event, context, callback) => {
    const parsedBody = parseBody(event, callback)

    const { error: bodyError, value: validBody } = schema.validate(parsedBody)
    if (bodyError) {
        return createResponse({ statusCode: 400, body: { message: 'Bad request: Missing data.', error: bodyError.details } })
    }

    const { email } = validBody;
    let graphql: GraphQLClient = initializeHasura(callback, event, true);

    // Hot-loading implementation (Only initializes while performing cold-starts)
    if (!transport) {
        transport = initializeMail(callback)
    }

    const user = await getUserByEmail(graphql, email)
    if (!user) {
        return createResponse({ statusCode: 401, body: { message: 'Bad request: User does not exist' } })
    }

    const generatedToken = await insertResetToken(graphql, { user_id: user.id })
    if (!generatedToken) {
        return createResponse({ statusCode: 500, body: { message: 'Internal server error: Something went wrong trying to generate a token' } })
    }

    const link = `${APP_URL}/password-reset/${user.id}/${generatedToken.token}`;
    
    let confirmation = await sendMail(transport, {
        to: email,
        subject: `${APP_NAME} | Password reset link`,
        html:
            template(
                'https://www.myfinance.nl/wp-content/uploads/2018/09/logo-placeholder.png',
                `${user.first_name}`,
                'A password reset request has been generated.',
                'Please ignore this email if this was not you.',
                'RESET',
                link,
                'support@profitflow.nl',
                'Maagdenburgstraat 14, Deventer'
            )

    })
    if (!confirmation.messageId) {
        return createResponse({ statusCode: 500, body: { message: 'Internal server error: Mail could not be sent.' } })
    }

    return createResponse({ statusCode: 200, body: { message: 'Password reset link has been succesfully generated, check your email!' } })
};


export { handler };