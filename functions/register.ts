import passwordComplexity from 'joi-password-complexity';
import Joi from 'joi';
import { Handler } from "@netlify/functions";
import { GraphQLClient } from 'graphql-request';
import { Transporter } from 'nodemailer'
import SMTPTransport from "nodemailer/lib/smtp-transport";
import { checkUserExist, createUser, initializeHasura, signToken } from "./lib/hasura-util";
import { initializeMail, sendMail } from "./lib/mail-util";
import { parseBody, createResponse } from "./lib/util";
import { template } from './lib/mail';
let { APP_URL, APP_NAME } = process.env;

let transport: Transporter<SMTPTransport.SentMessageInfo>;

const schema = Joi.object({
  email: Joi.string().email().required(),
  password: passwordComplexity().required(),
  first_name: Joi.string().required(),
  last_name: Joi.string().required(),
  role: Joi.string().required(),
})


/**
 * Registers an user, sends the user an email and returns a valid JWT token
 * 
 * Body:
 * @param email: String
 * @param password: String
 * @param first_name: String
 * @param last_name: String
 * @returns token
 */
const handler: Handler = async (event, context, callback) => {
  const parsedBody = parseBody(event, callback)

  const { error: bodyError, value: validBody } = schema.validate(parsedBody)
  if (bodyError) {
    return createResponse({ statusCode: 400, body: { message: 'Bad request: Missing data.', error: bodyError.details } })
  }

  const { email, password, first_name, last_name, role } = validBody;
  let graphql: GraphQLClient = initializeHasura(callback, event);


  // Hot-loading implementation (Only initializes while performing cold-starts)
  if (!transport) {
    transport = initializeMail(callback)
  }

  const existingUser = await checkUserExist(graphql, email)
  if (existingUser) {
    return createResponse({ statusCode: 401, body: { message: 'Bad request: User already exists' } })
  }

  const createdUser = await createUser(graphql, { email, password, first_name, last_name, role })
  if (!createdUser) {
    return createResponse({ statusCode: 500, body: { message: 'Internal server error: Something went wrong trying to upload the file to the backend.' } })
  }

  let confirmation = await sendMail(transport, {
    to: email,
    subject: `Welcome to ${process.env.APP_NAME}, ${first_name} ${last_name}!`,
    html: template(
      'https://www.myfinance.nl/wp-content/uploads/2018/09/logo-placeholder.png',
      `${first_name}`,
      `You have succesfully registered an account at ${APP_NAME}`,
      'Please notify an administrator if this was not you.',
      'LOGIN',
      `${APP_URL}/login`,
      'support@profitflow.nl',
      'Maagdenburgstraat 14, Deventer'
  )

  })
  if (!confirmation.messageId) {
    return createResponse({ statusCode: 500, body: { message: 'Internal server error: Mail could not be sent.' } })
  }

  const token = await signToken({ user: createdUser })
  if (!token) {
    return createResponse({ statusCode: 500, body: { message: 'Internal server error: Something went wrong trying to sign your token. Your account has been created. Please login manually.' } })
  }

  return createResponse({ statusCode: 201, body: { token } })
};

export { handler };