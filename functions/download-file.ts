import Joi from 'joi';
import { createSignedUrl, initializeS3 } from './lib/s3-util';
import { Handler } from "@netlify/functions";
import { parseBody, createResponse } from './lib/util';
import { GraphQLClient } from 'graphql-request';
import { getFilePath, initializeHasura } from './lib/hasura-util';

let S3: AWS.S3;

const schema = Joi.object({
    id: Joi.string().uuid().required(),
})

const handler: Handler = async (event, context, callback) => {
    const parsedBody = parseBody(event, callback)

    const { error: bodyError, value: validBody } = schema.validate(parsedBody)
    if (bodyError) {
        return createResponse({ statusCode: 400, body: { message: 'Bad request: Missing data.', error: bodyError.details } })
    }

    const { id } = validBody;

    let graphql: GraphQLClient = initializeHasura(callback, event);

    // Hot-loading implementation (Only initializes while performing cold-starts)
    if (!S3) {
        S3 = initializeS3(callback);
    }

    const file = await getFilePath(graphql, { id });

    if (!file) {
        return createResponse({ statusCode: 401, body: { message: 'Unauthorized: You are not allowed to download this file.' } })
    }

    const presignedUrl = await createSignedUrl(S3, { path: file.path, type: 'getObject' })
    if (!presignedUrl) {
        return createResponse({ statusCode: 500, body: { message: 'Internal server error: Something went wrong trying to get a signed URL from AWS' } })
    }

    return createResponse({ statusCode: 200, body: { presignedUrl: file.path } })
};

export { handler };