import { Handler } from "@netlify/functions";
import fetch from "node-fetch";

interface IBody {
    user_id: string,
    registration_ids: string[]
}

const handler: Handler = async (event, context) => {
    let { FIREBASE_API_KEY, FIREBASE_PROJECT_ID, FIREBASE_API_URL } = process.env;
    const { user_id, registration_ids }: IBody = JSON.parse(event.body)

    // Make sure all fields are given
    if (!user_id || !registration_ids || registration_ids.length < 1) {
        return {
            statusCode: 400,
            body: 'Bad request: Missing data.',
            headers: {
                'Content-Type': 'application/json'
            }
        }
    }

    const data: any = await fetch(`${FIREBASE_API_URL}/notification`, {
        method: 'post',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `key=${FIREBASE_API_KEY}`,
            project_id: FIREBASE_PROJECT_ID
        },
        body: JSON.stringify({
            operation: 'create',
            notification_key_name: user_id,
            registration_ids
        }),
    }).then(res => res.json())
        .catch(err => {
            return {
                statusCode: 400,
                body: err.error,
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        })

    if (data.error) {
        return {
            statusCode: 400,
            body: data.error,
            headers: {
                'Content-Type': 'application/json'
            }
        }
    }

    const notification_key = data.notification_key

    if (!notification_key) {
        return {
            statusCode: 400,
            body: 'Internal server error: Something went wrong at GCF, no notification_key found.',
            headers: {
                'Content-Type': 'application/json'
            }
        }
    }

    return {
        statusCode: 201,
        body: JSON.stringify({ notification_key }),
        headers: {
            'Content-Type': 'application/json'
        }
    };
};

export { handler };