import jwt from 'jsonwebtoken';
import { HandlerCallback } from "@netlify/functions";
import { gql, GraphQLClient } from "graphql-request";
import { createResponse } from "./util";
import bcrypt from 'bcrypt';
import crypto from 'crypto'
import { Event } from '@netlify/functions/dist/function/event';
let { VITE_HASURA_API, VITE_HASURA_ADMIN, PRIVATE_KEY } = process.env;

export const initializeHasura = (callback: HandlerCallback, event: Event, admin: boolean = false): GraphQLClient => {
    if (!VITE_HASURA_API || !VITE_HASURA_ADMIN) {
        throw callback(null, createResponse({ statusCode: 500, body: { message: 'Internal server error: Missing hasura environment variables.' } }))
    }
    let headers;
    if (!admin && event.headers.authorization) {
        headers = {
            authorization: event.headers.authorization
        }
    }
    if (admin) {
        headers = {
            'x-hasura-admin-secret': VITE_HASURA_ADMIN
        }
    }
    return new GraphQLClient(VITE_HASURA_API, { headers });
}

export const signToken = ({ user }: { user: IUser }): string => {
    PRIVATE_KEY = PRIVATE_KEY!.replace(/\\n/gm, '\n')

    const token = jwt.sign({
        sub: String(user.id),
        first_name: user.first_name,
        last_name: user.last_name,
        email: user.email,
        "https://hasura.io/jwt/claims": {
            'x-hasura-user-id': String(user.id),
            'x-hasura-allowed-roles': [user.role],
            'x-hasura-default-role': user.role,
        }
    }, PRIVATE_KEY, { algorithm: 'RS256', expiresIn: '1d' })
    return token;
}


export const getUserByEmail = async (graphql: GraphQLClient, email: string): Promise<IUser> => {
    return (await graphql.request(GETUSERBYEMAIL, { email }))?.auth_user[0]
}

export const checkUserExist = async (graphql: GraphQLClient, email: string) => {
    const user: IUser = (await graphql.request(GETUSERBYEMAIL, { email }))?.auth_user[0]
    return user
}

export const createUser = async (graphql: GraphQLClient, { email, password, first_name, last_name, role }:
    { email: string, password: string, first_name: string, last_name: string, role: string }
) => {
    const hashedPassword = await bcrypt.hash(password, 10);
    return await graphql.request(REGISTERUSER, {
        email,
        password: hashedPassword,
        first_name,
        last_name,
        role,
    })
}

export const insertFile = async (graphql: GraphQLClient, { path, size, secure }: { path: string, size: number, secure: boolean }) => {
    const created = (await graphql.request(UPLOADFILE, {
        path,
        size,
        secure
    }))
    return created;
}

export const GETUSERBYEMAIL = gql`
    query GETUSERBYEMAIL($email: String) {
        auth_user(where: {email: {_eq: $email}}, limit: 1) {
            password
            first_name
            last_name
            email
            id
            role
      }
    }
`

export interface IResetToken {
    expires_at: string;
    user: IUser
}

export interface IFile {
    id: string;
    path: string;
}

export const getUser = async (graphql: GraphQLClient, { user_id }: { user_id: string }) => {
    const user: IUser = (await graphql.request(GETUSERBYPK, { user_id }))?.auth_user_by_pk
    return user
}

export const getFilePath = async (graphql: GraphQLClient, { id }: { id: string }) => {
    const file: IFile = (await graphql.request(GETFILE, { id }))?.storage_file_by_pk
    return file
}


export const getToken = async (graphql: GraphQLClient, { user_id, token }: { user_id: number, token: string }) => {
    const resetToken: IResetToken = (await graphql.request(GETRESETTOKEN, { user_id, token }))?.auth_reset_token[0]
    return resetToken
}

export const changePassword = async (graphql: GraphQLClient, { user_id, password }: { user_id: number, password: string }) => {
    const hashedPassword = await bcrypt.hash(password, 10);
    return await graphql.request(CHANGEPASSWORD, {
        user_id,
        password: hashedPassword,
    })
}

export const insertResetToken = async (graphql: GraphQLClient, { user_id }: { user_id: number }) => {
    const token = crypto.randomBytes(32).toString("hex");
    return (await graphql.request(INSERTRESETTOKEN, {
        user_id,
        token,
    }))?.insert_auth_reset_token_one;
}

export const removeResetToken = async (graphql: GraphQLClient, { user_id }: { user_id: number }) => {
    const token = crypto.randomBytes(32).toString("hex");
    return (await graphql.request(REMOVERESETTOKEN, {
        user_id,
    }))?.delete_auth_reset_token_by_pk;
}


export interface IUser {
    password: string;
    id: number;
    role: string;
    email: string;
    first_name: string;
    last_name: string;
}


const REGISTERUSER = gql`
  mutation REGISTERUSER($email: String!, $first_name: String!, $last_name: String!, $password: String!, $role: auth_role_enum!) {
    insert_auth_user_one(object: {email: $email, first_name: $first_name, last_name: $last_name, password: $password, role: $role}) {
        password
        first_name
        email
        last_name
        id
        role
    }
  }
`

const INSERTRESETTOKEN = gql`
    mutation INSERTRESETTOKEN($token: String!, $user_id: Int!) {
        insert_auth_reset_token_one(object: {token: $token, user_id: $user_id}, on_conflict: {constraint: reset_token_pkey, update_columns: [expires_at, token]}) {
            token
        }
    }
`

const UPLOADFILE = gql`
    mutation UPLOADFILE($size: Int!, $path: String!, $confirmed: Boolean = false, $secure: Boolean!) {
        insert_storage_file_one(object: {path: $path, size: $size, secure: $secure, confirmed: $confirmed}, on_conflict: {constraint: attachment_pkey, update_columns: [path, size, secure, confirmed]}) {
            path
        }
    }
`

const GETUSERBYPK = gql`
  query GETUSERBYPK($user_id: Int!) {
    auth_user_by_pk(id: $user_id) {
      id
      email
      password
    }
  }
`


const GETFILE = gql`
    query GETFILE($id: uuid!) {
        storage_file_by_pk(id: $id) {
            id
            path
        }
    }
`

const GETRESETTOKEN = gql`
    query GETRESETTOKEN($user_id: Int!, $token: String) {
        auth_reset_token(where: {user_id: {_eq: $user_id}, token: {_eq: $token}}) {
            expires_at
            user {
                email
                first_name
                last_name
                id
            }
        }
    }
`

const CHANGEPASSWORD = gql`
  mutation CHANGEPASSWORD($user_id: Int!, $password: String) {
    update_auth_user_by_pk(pk_columns: {id: $user_id}, _set: {password: $password}) {
        id
    }
  }
`

const REMOVERESETTOKEN = gql`
    mutation REMOVERESETTOKEN($user_id: Int!) {
        delete_auth_reset_token_by_pk(user_id: $user_id) {
            user_id
        }
    }
`



