import { HandlerCallback } from "@netlify/functions"
import { Event } from "@netlify/functions/dist/function/event";
import jwt from 'jsonwebtoken';

let { PUBLIC_KEY } = process.env;
PUBLIC_KEY = PUBLIC_KEY!.replace(/\\n/gm, '\n')

export const parseBody = (event: Event, callback: HandlerCallback): any => {
    let body = event.body
    let parsedBody;
    try {
        if (!body) {
            throw Error('No body')
        }
        parsedBody = JSON.parse(body)
        if (parsedBody.input) {
            parsedBody = parsedBody.input
        }
    } catch (e) {
        return callback(null, createResponse({ statusCode: 400, body: { message: 'Bad request: No body.' } }))
    }

    return parsedBody
}

export const createResponse = ({ statusCode, body }: { statusCode: number, body: any }) => {
    body = JSON.stringify(body)
    return {
        statusCode,
        body,
        headers: {
            'Content-Type': 'application/json'
        }
    }
}

export const validateToken = (token: string, callback: HandlerCallback): IToken => {
    let verified;
    try {
        verified = jwt.verify(token, PUBLIC_KEY!);
        if (verified) {
            const decoded = jwt.decode(token, { complete: true });
            const payload = decoded?.payload as IToken;

            if (Date.now() >= payload.exp * 1000) {
                throw Error('Unauthorized: Token is expired.')
            }

            return payload;
        } else {
            throw Error('Unauthorized: Token is invalid.')
        }
    } catch (e) {
        if (e.message) {
            callback(null, createResponse({ statusCode: 401, body: { message: e.message } }))
        }
        callback(null, createResponse({ statusCode: 401, body: { message: 'Unauthorized: Token is invalid.' } }))
    }
}

export const getBearerToken = (event: Event) => {
    return event.headers.authorization?.replace('Bearer ', '');
}

export const isAdmin = (decoded: IToken) => {
    if (decoded && decoded["https://hasura.io/jwt/claims"]["x-hasura-allowed-roles"].includes('admin')) {
        return true
    }
    return false
}

export interface IToken {
    exp: number,
    iat: number,
    sub: string,
    first_name: string,
    last_name: string,
    email: string,
    "https://hasura.io/jwt/claims": {
        'x-hasura-user-id': string,
        'x-hasura-team-id': string,
        'x-hasura-allowed-roles': string[],
        'x-hasura-default-role': string,
    }
}