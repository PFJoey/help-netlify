import { HandlerCallback } from "@netlify/functions";
import AWS from "aws-sdk";
import { createResponse } from "./util";

const { S3_BUCKET, S3_ENDPOINT, S3_SECRET, S3_KEY } = process.env;

export const initializeS3 = (callback: HandlerCallback) => {
    if (!S3_BUCKET || !S3_ENDPOINT || !S3_SECRET || !S3_KEY) {
        throw callback(null, createResponse({ statusCode: 500, body: { message: 'Internal server error: Missing S3 environment variables.' } }))
    }
    return new AWS.S3({
        endpoint: new AWS.Endpoint(S3_ENDPOINT),
        credentials: {
            accessKeyId: S3_KEY,
            secretAccessKey: S3_SECRET,
        }
    })
}

export const createSignedUrl = async (S3: AWS.S3, { path, type }: { path: string, type: 'putObject' | 'getObject' }) => {
    const presignedUrl = await S3.getSignedUrl(type, {
        Bucket: S3_BUCKET,
        Expires: 60 * 15,
        Key: path,
    });
    return presignedUrl
}