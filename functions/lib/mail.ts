export const template = (logo: string, user: string, message:string, subtitle: string, button: string, buttonlink: string, mailto: string, address: string) => {
    return `
    <!DOCTYPE html>
<html
    style="box-sizing: border-box; background-color: #fff; font-size: 14px; -moz-osx-font-smoothing: grayscale; -webkit-font-smoothing: antialiased; min-width: 300px; overflow-x: hidden; overflow-y: scroll; text-rendering: optimizeLegibility; text-size-adjust: 100%; margin: 0; padding: 0;">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">



    <style>
        body {
            margin: 0;
            padding: 0;
        }

        img {
            height: auto;
            max-width: 100%;
        }

        body {
            font-family: -apple-system, blinkmacsystemfont, "Segoe UI", roboto, oxygen-sans, ubuntu, cantarell, "Helvetica Neue", sans-serif;
        }

        body {
            color: #525761;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
        }

        img {
            height: auto;
            max-width: 100%;
        }

        body.has-navbar-fixed-top {
            padding-top: 3.25rem;
        }

        body.has-navbar-fixed-bottom {
            padding-bottom: 3.25rem;
        }

        body {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        img {
            -ms-interpolation-mode: bicubic;
        }

        body {
            width: 100% !important;
            height: 100% !important;
            padding: 0 !important;
            margin: 0 !important;
            background-color: #e9ecef;
        }

        img {
            height: auto;
            line-height: 100%;
            text-decoration: none;
            border: 0;
            outline: none;
        }
    </style>
</head>

<body
    style='box-sizing: inherit; font-family: -apple-system, blinkmacsystemfont, "Segoe UI", roboto, oxygen-sans, ubuntu, cantarell, "Helvetica Neue", sans-serif; color: #525761; font-size: 1rem; font-weight: 400; line-height: 1.5; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; width: 100% !important; height: 100% !important; margin: 0; padding: 0;'
    bgcolor="#e9ecef">

    <div class="preheader"
        style="box-sizing: inherit; display: none; max-width: 0; max-height: 0; overflow: hidden; font-size: 1px; line-height: 1px; color: #fff; opacity: 0;">

    </div>

    <table border="0" cellpadding="0" cellspacing="0" width="100%"
        style="box-sizing: inherit; border-collapse: collapse !important; border-spacing: 0; width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; mso-table-rspace: 0pt; mso-table-lspace: 0pt;">

        <tr style="box-sizing: inherit;">
            <td align="center" bgcolor="#e9ecef"
                style="box-sizing: inherit; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; mso-table-rspace: 0pt; mso-table-lspace: 0pt; padding: 0;"
                valign="top">
                <!--[if (gte mso 9)|(IE)]>
      <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
        <tr>
          <td align="center" valign="top" width="600">
      <![endif]-->
                <table border="0" cellpadding="0" cellspacing="0" width="100%"
                    style="max-width: 600px; box-sizing: inherit; border-collapse: collapse !important; border-spacing: 0; width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; mso-table-rspace: 0pt; mso-table-lspace: 0pt;">
                    <tr style="box-sizing: inherit;">
                        <td align="center" valign="top"
                            style="box-sizing: inherit; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; mso-table-rspace: 0pt; mso-table-lspace: 0pt; padding: 36px 24px;">
                            <img alt="logo"
                                src=${logo}
                                style="box-sizing: inherit; height: auto; max-width: 100%; -ms-interpolation-mode: bicubic; line-height: 100%; text-decoration: none; outline: none; border: 0;">
                        </td>
                    </tr>
                </table>
                <!--[if (gte mso 9)|(IE)]>
      </td>
      </tr>
      </table>
      <![endif]-->
            </td>
        </tr>

        <tr style="box-sizing: inherit;">
            <td align="center" bgcolor="#e9ecef"
                style="box-sizing: inherit; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; mso-table-rspace: 0pt; mso-table-lspace: 0pt; padding: 0;"
                valign="top">
                <!--[if (gte mso 9)|(IE)]>
      <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
        <tr>
          <td align="center" valign="top" width="600">
      <![endif]-->
                <table border="0" cellpadding="0" cellspacing="0" width="100%"
                    style="max-width: 600px; box-sizing: inherit; border-collapse: collapse !important; border-spacing: 0; width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; mso-table-rspace: 0pt; mso-table-lspace: 0pt;">

                    <tr style="box-sizing: inherit;">
                        <td bgcolor="#ffffff" align="left"
                            style="font-size: 16px; line-height: 24px; border-top-color: #d4dadf; border-top-width: 3px; border-top-style: solid; box-sizing: inherit; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; mso-table-rspace: 0pt; mso-table-lspace: 0pt; padding: 24px;"
                            valign="top">
                            <h1 class="title"
                                style="font-size: 2rem; font-weight: 600; box-sizing: inherit; word-break: break-word; color: #000; line-height: 1.125; margin: 0 0 1.5rem; padding: 0;">
                                Hello ${user},</h1>
                            <p style="box-sizing: inherit; margin: 0; padding: 0;">${message}</p>
                        </td>
                    </tr>

                    <tr style="box-sizing: inherit;">
                        <td bgcolor="#ffffff" align="left"
                            style="font-size: 16px; box-sizing: inherit; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; mso-table-rspace: 0pt; mso-table-lspace: 0pt; padding: 8px 24px 0px;"
                            valign="top">
                            ${subtitle}
                        </td>
                    </tr>

                    <tr style="box-sizing: inherit;">
                        <td align="left" bgcolor="#ffffff"
                            style="box-sizing: inherit; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; mso-table-rspace: 0pt; mso-table-lspace: 0pt; padding: 0;"
                            valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%"
                                style="box-sizing: inherit; border-collapse: collapse !important; border-spacing: 0; width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; mso-table-rspace: 0pt; mso-table-lspace: 0pt;">
                                <tr style="box-sizing: inherit;">
                                    <td align="center" bgcolor="#ffffff"
                                        style="box-sizing: inherit; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; mso-table-rspace: 0pt; mso-table-lspace: 0pt; padding: 12px;"
                                        valign="top">
                                        <table border="0" cellpadding="0" cellspacing="0"
                                            style="box-sizing: inherit; border-collapse: collapse !important; border-spacing: 0; width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; mso-table-rspace: 0pt; mso-table-lspace: 0pt;">
                                            <tr style="box-sizing: inherit;">
                                                <td align="center"
                                                    style="box-sizing: inherit; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; mso-table-rspace: 0pt; mso-table-lspace: 0pt; padding: 0;"
                                                    valign="top">
                                                    <a class="button is-primary"
                                                        href="${buttonlink}"
                                                        style="-webkit-touch-callout: none; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none; -moz-appearance: none; -webkit-appearance: none; align-items: center; border-radius: 2px; box-shadow: 0 3px 1px rgba(111,118,130,0.2); display: inline-flex; font-size: 1rem; height: 2.25em; justify-content: center; line-height: 2.25em; position: relative; vertical-align: top; box-sizing: inherit; color: #fff; cursor: pointer; text-decoration: none; background-color: #5c4ee5; text-align: center; white-space: nowrap; font-weight: 700; min-width: auto; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding: calc(0.375em - 1px) 1em; border: 1px solid #3322de;">
                                                        ${button}
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>



                </table>
                <!--[if (gte mso 9)|(IE)]>
      </td>
      </tr>
      </table>
      <![endif]-->
            </td>
        </tr>

        <tr style="box-sizing: inherit;">
            <td align="center" bgcolor="#e9ecef"
                style="box-sizing: inherit; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; mso-table-rspace: 0pt; mso-table-lspace: 0pt; padding: 24px;"
                valign="top">
                <!--[if (gte mso 9)|(IE)]>
      <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
        <tr>
          <td align="center" valign="top" width="600">
      <![endif]-->
                <table border="0" cellpadding="0" cellspacing="0" width="100%"
                    style="max-width: 600px; box-sizing: inherit; border-collapse: collapse !important; border-spacing: 0; width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; mso-table-rspace: 0pt; mso-table-lspace: 0pt;">

                    <tr style="box-sizing: inherit;">
                        <td align="center" bgcolor="#e9ecef"
                            style="font-size: 14px; line-height: 20px; color: #666; box-sizing: inherit; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; mso-table-rspace: 0pt; mso-table-lspace: 0pt; padding: 12px 24px;"
                            valign="top">
                            <p style="box-sizing: inherit; margin: 0; padding: 0;">Need
                                help? <a href="${mailto}"
                                    style="box-sizing: inherit; color: black; cursor: pointer; text-decoration: underline; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">Contact
                                    us</a></p>
                        </td>
                    </tr>

                </table>
                <!--[if (gte mso 9)|(IE)]>
      </td>
      </tr>
      </table>
      <![endif]-->
            </td>
        </tr>

    </table>

</body>

</html>
    
`
}
