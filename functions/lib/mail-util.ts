let { MAIL_SERVER, MAIL_PORT, MAIL_SECURE, MAIL_USERNAME, MAIL_PASSWORD, APP_URL, APP_NAME, MAIL_FROM } = process.env;
import { HandlerCallback } from '@netlify/functions';
import nodemailer, { Transporter } from 'nodemailer'
import SMTPTransport from 'nodemailer/lib/smtp-transport';
import { createResponse } from './util';

export const initializeMail = (callback: HandlerCallback) => {
    if (!MAIL_SERVER || !MAIL_PORT || !MAIL_SECURE || !MAIL_USERNAME || !MAIL_PASSWORD || !MAIL_FROM) {
        throw callback(null, createResponse({ statusCode: 500, body: { message: 'Internal server error: Missing mail environment variables.' } }))
    }

    return nodemailer.createTransport({
        host: MAIL_SERVER,
        port: Number(MAIL_PORT),
        secure: Boolean(MAIL_SECURE),
        auth: {
            user: MAIL_USERNAME,
            pass: MAIL_PASSWORD
        },
        from: MAIL_FROM,
    });
}

export const sendMail = async (transporter: Transporter<SMTPTransport.SentMessageInfo>, { to, subject, html }: { to: string, subject: string, html: string }) => {
    let confirmation = await transporter.sendMail({
        to,
        subject,
        html,
        from: MAIL_FROM
    });
    return confirmation
}
