import { Handler } from "@netlify/functions";
import fetch from "node-fetch";

interface IBody {
    user_id: string,
    title: string;
    body: string;
}

const handler: Handler = async (event, context) => {
    let { FIREBASE_API_KEY, FIREBASE_PROJECT_ID, FIREBASE_API_URL } = process.env;
    const { user_id, title, body }: IBody = JSON.parse(event.body)

    // Make sure all fields are given
    if (!user_id || !title || !body) {
        return {
            statusCode: 400,
            body: 'Bad request: Missing data.',
            headers: {
                'Content-Type': 'application/json'
            }
        }
    }


    const notificationKeyResponse: any = await fetch(`${FIREBASE_API_URL}/notification?notification_key_name=${user_id}`, {
        method: 'get',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `key=${FIREBASE_API_KEY}`,
            project_id: FIREBASE_PROJECT_ID
        },
    }).then(res => res.json()).catch(err => {
        return {
            statusCode: 400,
            body: err.error,
            headers: {
                'Content-Type': 'application/json'
            }
        }
    })

    const notification_key = notificationKeyResponse.notification_key

    if (!notification_key) {
        return {
            statusCode: 400,
            body: 'Internal server error: Something went wrong, no notification_key found.',
            headers: {
                'Content-Type': 'application/json'
            }
        }
    }

    const sendMessageResponse: any = await fetch(`${FIREBASE_API_URL}/send`, {
        method: 'post',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `key=${FIREBASE_API_KEY}`,
            project_id: FIREBASE_PROJECT_ID
        },
        body: JSON.stringify({
            to: notification_key,
            notification: {
                title,
                body
            }
        }),
    }).then(res => res.json()).catch(err => {
        return {
            statusCode: 400,
            body: err.error,
            headers: {
                'Content-Type': 'application/json'
            }
        }
    })

    if (sendMessageResponse.error) {
        return {
            statusCode: 400,
            body: sendMessageResponse.error,
            headers: {
                'Content-Type': 'application/json'
            }
        }
    }


    return {
        statusCode: 200,
        body: JSON.stringify({ success: sendMessageResponse.success, failure: sendMessageResponse.failure }),
        headers: {
            'Content-Type': 'application/json'
        }
    };
};

export { handler };