import { Handler } from "@netlify/functions";
import { GraphQLClient } from 'graphql-request';
import bcrypt from 'bcrypt';
import { parseBody, createResponse } from "./lib/util";
import { getUserByEmail, initializeHasura, signToken } from "./lib/hasura-util";
import Joi from 'joi'

const schema = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().required()
})

/**
 * Signs a token depending on correct login credentials
 * 
 * Body:
 * @param email: String
 * @param password: String
 * @returns token
 */
const handler: Handler = async (event, context, callback) => {
  const parsedBody = parseBody(event, callback)

  const { error: bodyError, value: validBody } = schema.validate(parsedBody)
  if (bodyError) {
    return createResponse({ statusCode: 400, body: { message: 'Bad request: Missing data.', error: bodyError.details } })
  }

  const { email, password } = validBody;

  let graphql: GraphQLClient = initializeHasura(callback, event, true);

  // Check if the user exists
  const user = await getUserByEmail(graphql, email)
  if (!user) {
    return createResponse({ statusCode: 401, body: { message: 'Unauthorized: Credentials entered are invalid.' } })
  }

  // Check if the password matches
  const isPasswordCorrect = await bcrypt.compare(password, user.password)
  if (!isPasswordCorrect) {
    return createResponse({ statusCode: 401, body: { message: 'Unauthorized: Credentials entered are invalid.' } })
  }

  // Sign token
  const token = signToken({ user })
  if (!token) {
    return createResponse({ statusCode: 500, body: { message: 'Internal server error: Something went wrong trying to sign your token.' } })
  }

  return createResponse({ statusCode: 200, body: { token } })
};



export { handler };