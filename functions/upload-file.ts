import Joi from 'joi';
import { parseBody, createResponse } from './lib/util';
import { Handler } from "@netlify/functions";
import { GraphQLClient } from 'graphql-request';
import AWS from 'aws-sdk'
import { initializeHasura, insertFile } from './lib/hasura-util';
import { initializeS3, createSignedUrl } from './lib/s3-util';

let S3: AWS.S3;

const schema = Joi.object({
    path: Joi.string().required(),
    secure: Joi.boolean().required(),
    size: Joi.number().required(),
})


const handler: Handler = async (event, context, callback) => {
    const parsedBody = parseBody(event, callback)

    const { error: bodyError, value: validBody } = schema.validate(parsedBody)
    if (bodyError) {
        return createResponse({ statusCode: 400, body: { message: 'Bad request: Missing data.', error: bodyError.details } })
    }

    const { path, secure, size } = validBody;
    let graphql: GraphQLClient = initializeHasura(callback, event);

    // Hot-loading implementation (Only initializes while performing cold-starts)
    if (!S3) {
        S3 = initializeS3(callback);
    }

    const created = await insertFile(graphql, { path, size, secure })
    if (!created) {
        return createResponse({ statusCode: 500, body: { message: 'Internal server error: Something went wrong trying to store the file path.' } })
    }

    const presignedUrl = await createSignedUrl(S3, { path, type: 'putObject' })
    if (!presignedUrl) {
        return createResponse({ statusCode: 500, body: { message: 'Internal server error: Something went wrong trying to get a signed URL from AWS' } })
    }

    return createResponse({ statusCode: 200, body: { presignedUrl } })
};


export { handler };