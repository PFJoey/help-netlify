import passwordComplexity from 'joi-password-complexity';
import Joi from 'joi';
import { Handler } from "@netlify/functions";
import { GraphQLClient } from 'graphql-request';
import { Transporter } from 'nodemailer'
import { parseBody, createResponse, validateToken, isAdmin, getBearerToken } from "./lib/util";
import SMTPTransport from "nodemailer/lib/smtp-transport";
import { changePassword, getUser, initializeHasura } from "./lib/hasura-util";
import { initializeMail, sendMail } from "./lib/mail-util";
import bcrypt from 'bcrypt';
import { template } from './lib/mail';

let { APP_URL, APP_NAME } = process.env;

let transport: Transporter<SMTPTransport.SentMessageInfo>;

const schema = Joi.object({
    user_id: Joi.string().email().optional(),
    password: passwordComplexity().required(),
    current_password: Joi.string().required(),
})


/**
 * Changes the password by requesting the current and new password. Grabs the user_id from JWT, OR if role = admin it will allow an override through the body
 * 
 * Body:
 * @param user_id: Int (optional)
 * @param password: String
 * @param current_password: String
 * @returns message
 */
const handler: Handler = async (event, context, callback) => {
    const parsedBody = parseBody(event, callback)

    const { error: bodyError, value: validBody } = schema.validate(parsedBody)
    if (bodyError) {
        return createResponse({ statusCode: 400, body: { message: 'Bad request: Missing data.', error: bodyError.details } })
    }

    let { user_id, password, current_password } = validBody;



    const token = getBearerToken(event)
    console.log(token);
    
    if (!token) {
        return createResponse({ statusCode: 401, body: { message: 'Unauthorized: You are not logged in.' } })
    }

    const decoded = validateToken(token, callback);
    user_id = decoded['https://hasura.io/jwt/claims']['x-hasura-user-id']

    if (!user_id || !password || !current_password) {
        return createResponse({ statusCode: 400, body: { message: 'Bad request: Missing data.' } })
    }

    if (password == current_password) {
        return createResponse({ statusCode: 401, body: { message: 'Bad request: Password can not be the same as the current password.' } })
    }

    let graphql: GraphQLClient = initializeHasura(callback, event);


    // Hot-loading implementation (Only initializes while performing cold-starts)
    if (!transport) {
        transport = initializeMail(callback)
    }

    const user = await getUser(graphql, { user_id })
    if (!user) {
        return createResponse({ statusCode: 400, body: { message: 'Bad request: User does not exist' } })
    }

    const isPasswordCorrect = await bcrypt.compare(current_password, user.password)
    if (!isPasswordCorrect) {
        return createResponse({ statusCode: 401, body: { message: 'Unauthorized: Current password is invalid.' } })
    }

    const changedPassword = await changePassword(graphql, { user_id, password });
    if (!changedPassword) {
        return createResponse({ statusCode: 500, body: { message: 'Internal server error: Something went wrong trying to change the password' } })
    }

    let confirmation = await sendMail(transport, {
        to: user.email,
        subject: `${APP_NAME} | Your password has been changed`,
        html: template(
            'https://www.myfinance.nl/wp-content/uploads/2018/09/logo-placeholder.png',
            `${user.first_name}`,
            'Your password has been succesfully changed.',
            'Please notify an administrator if this was not you.',
            'LOGIN',
            `${APP_URL}/login`,
            'support@profitflow.nl',
            'Maagdenburgstraat 14, Deventer'
        )
    })
    if (!confirmation.messageId) {
        return createResponse({ statusCode: 500, body: { message: 'Internal server error: Mail could not be sent.' } })
    }

    return createResponse({ statusCode: 200, body: { message: 'Password has been succesfully changed!' } })
};

export { handler };